package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class FactorialTest {

    @Test
    public void factorialTest(){
        Sestavit ses = new Sestavit();

        assertEquals(1, ses.factorial(0));
        assertEquals(1, ses.factorial(1));
        assertEquals(24, ses.factorial(4));
        assertEquals(720, ses.factorial(6));
    }
}
